<?php
/**
 * Display payment form.
 */
?>

<?php if($use_iframe): ?>
	<iframe id="wc_payment_iframe" name="wc_payment_iframe" width="405" height="479" style="border: 0;" src="<?php echo esc_attr($payment_link); ?>"></iframe>
<?php endif;?>

<?php if($use_apple): ?>
    <div class="apple-pay-button-container">
        <div class="apple-pay-button"></div>
    </div>
    <style>
    /* CSS */
    .apple-pay-button-container .apple-pay-button {
        display: inline-block;
        -webkit-appearance: -apple-pay-button;
        -apple-pay-button-style: <?php echo $apple_data['apple_button_style']; ?>;
        width: 240px;
        height: 50px;
    }
    .apple-pay-button-container {
        width: 100%;
        text-align: center;
        margin: 25px 5px;
    }
    </style>
    <script src="<?php echo $apple_data['apple_js_file']; ?>"></script>
    <script type="text/javascript">
    if(window.ApplePaySession) {
        if(ApplePaySession.canMakePayments()) {
            var applePayClient = ApplePayClient.initialize({
                access_token: '<?php echo $apple_data['mobile_access_token']; ?>',
                api_username: '<?php echo $apple_data['api_username']; ?>',
                merchant_id: '<?php echo $apple_data['merchant_id']; ?>',
                account_name: '<?php echo $apple_data['account_name']; ?>',
                env: '<?php echo $apple_data['payment_link']; ?>'
            });
            
            document.querySelector('.apple-pay-button').addEventListener('click', function() {
                applePayClient.createApplePayPayment({
                    payment_reference: '<?php echo $apple_data['payment_reference']; ?>',
                    country_code: '<?php echo $apple_data['country_code']; ?>',
                    currency_code: '<?php echo $apple_data['currency_code']; ?>',
                    payment_link: '<?php echo $apple_data['payment_link']; ?>',
                    total: {
                        label: '<?php echo $apple_data['total_label']; ?>',
                        amount: '<?php echo $apple_data['total_amount']; ?>'
                    }
                })
            });
        }
    }
    </script>
<?php endif;?>

<div class="ping-message-wrapper">
	<p class="status-message pending" style="display: none;"><?php esc_html_e('Please wait while we check your payment status…', 'everypay'); ?></p>
	<p class="status-message success" style="display: none;"><?php esc_html_e('Payment successful. Redirecting to success page.', 'everypay'); ?><br><?php echo str_replace(['{', '}'], ['<a href="' . $redirect_url . '">', '</a>'], esc_html__('If you are not automatically redirected within a few seconds click {here}.', 'everypay')); ?></p>
	<p class="status-message failed" style="display: none;"><?php esc_html_e('Payment failed.', 'everypay'); ?></p>
</div>