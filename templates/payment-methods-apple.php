<?php
/**
 * Display Apple Pay info
 */
?>

<div class="everypay-wrapper">
    <script src="<?php echo $apple_js_file; ?>"></script>
    <input type="hidden" name="<?php echo esc_attr($gateway_id); ?>[is_apple]" value="1"/>
    <div id="apple-pay-container-parent">
        <button class="apple-pay-checkout" type="plain" buttonstyle="<?php echo $apple_button_style; ?>"></button>
    </div>
</div>
<style>
.wc_payment_method.payment_method_<?php echo $gateway_id; ?> {
    display: none;
}
.apple-pay-checkout {
    display: inline-block;
    border-radius: 3pt;
    -webkit-appearance: -apple-pay-button;
    -apple-pay-button-style: <?php echo $apple_button_style; ?>;
    width: 100%;
    height: 40px;
}
.payment_box.payment_method_<?php echo $gateway_id; ?> {
    display: none !important;
}
</style>
<script type="text/javascript">
jQuery(function($) {
        const hidePlaceOrderGateways = <?php echo json_encode($hide_place_order_button_gateways); ?>;
        
        function checkApplePayAvailable() {
            if($('.wc_payment_method.payment_method_<?php echo $gateway_id; ?>').length > 0) {
                var applePayAvailable = false;
                if(window.ApplePaySession) {
                    if(ApplePaySession.canMakePayments()) {
                        applePayAvailable = true;
                    }
                }
                if(applePayAvailable) {
                    $('.wc_payment_method.payment_method_<?php echo $gateway_id; ?>').show();
                } else {
                    $('.wc_payment_method.payment_method_<?php echo $gateway_id; ?>').remove();
                    $('body').trigger('update_checkout');
                }
            }
        }
        checkApplePayAvailable();
        
        $('body').on('updated_checkout', function() {
                checkApplePayAvailable();
                checkPaymentMethod();
        });
        
        function checkPaymentMethod() {
            var payment_method = $('input[name=payment_method]:checked').val();
            if(payment_method == '<?php echo $gateway_id; ?>') {
                var applePayButton = $("button.apple-pay-checkout").detach();
                $('button#place_order').after(applePayButton);
                $('button#place_order').hide();
            } else {
                var applePayButton = $("button.apple-pay-checkout").detach();
                $('#apple-pay-container-parent').html(applePayButton);
                if(!hidePlaceOrderGateways.includes(payment_method)) {
                    $('button#place_order').show();
                }
            }
        }
        $('form.checkout').on( 'click', 'input[name=payment_method]', function() {
                checkPaymentMethod();
        });
        
        
        var form_checkout = $('form.checkout');
        
        form_checkout.on( 'checkout_place_order_<?php echo $gateway_id; ?>', function() {
                return false;
        });
        
        function blockOnSubmit( $form ) {
			var isBlocked = $form.data( 'blockUI.isBlocked' );

			if ( 1 !== isBlocked ) {
				$form.block({
					message: null,
					overlayCSS: {
						background: '#fff',
						opacity: 0.6
					}
				});
			}
		}
		
		function submit_error( error_message ) {
			$( '.woocommerce-NoticeGroup-checkout, .woocommerce-error, .woocommerce-message' ).remove();
			form_checkout.prepend( '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' + error_message + '</div>' ); // eslint-disable-line max-len
			form_checkout.removeClass( 'processing' ).unblock();
			form_checkout.find( '.input-text, select, input:checkbox' ).trigger( 'validate' ).trigger( 'blur' );
			scroll_to_notices();
			$( document.body ).trigger( 'checkout_error' , [ error_message ] );
		}
		
		function scroll_to_notices() {
			var scrollElement = $( '.woocommerce-NoticeGroup-updateOrderReview, .woocommerce-NoticeGroup-checkout' );

			if ( ! scrollElement.length ) {
				scrollElement = form_checkout;
			}
			$.scroll_to_notices( scrollElement );
		}
		
		handleApplePayClick = function() {
            $('form.checkout').submit();
            blockOnSubmit(form_checkout);
            setTimeout(function() {
                var returnValue = false;
                var oneoff = $.ajax({
                    url: wc_checkout_params.checkout_url,
                    dataType: "json",
                    data: $('form.checkout').serialize(),
                    method: "POST",
                    async: false,
                    success: function(result) {
                        if ( result.apple ) {
                            returnValue = result.apple;
                        } else if ( result.messages ) {
                            submit_error( result.messages );
                        } else {
                            submit_error( '<div class="woocommerce-error">' + wc_checkout_params.i18n_checkout_error + '</div>' ); // eslint-disable-line max-len
                        }
                    },
                    error:	function( jqXHR, textStatus, errorThrown ) {
                        submit_error( '<div class="woocommerce-error">' + errorThrown + '</div>' );
                    }
                });
                
                if (returnValue) {
                    $('form.checkout').removeClass( 'processing' ).unblock();
                    var data = returnValue;
                    var applePayClient = ApplePayClient.initialize({
                        access_token: data.mobile_access_token,
                        api_username: data.api_username,
                        merchant_id: data.merchant_id,
                        account_name: data.account_name,
                        env: data.payment_link,
                    });
                    applePayClient.createApplePayPayment({
                        payment_reference: data.payment_reference,
                        country_code: data.country_code,
                        currency_code: data.currency_code,
                        payment_link: data.payment_link,
                        total: {
                            label: data.total_label,
                            amount: data.total_amount
                        }
                    });
                }
            }, 500);
        }
        if(window.ApplePaySession) {
            if(ApplePaySession.canMakePayments()) {
                document.querySelector('button.apple-pay-checkout').addEventListener('click', handleApplePayClick);
            }
        }
});
</script>