<?php
/**
 * Display Google Pay info
 */
?>

<div class="everypay-wrapper">
    <script src="<?php echo $google_js_file; ?>"></script>
    <input type="hidden" name="<?php echo esc_attr($gateway_id); ?>[is_google]" value="1"/>
    <div id="google-pay-container-parent"><div id="google-pay-container" class="google-pay-checkout"></div></div>
</div>
<style>
.google-pay-checkout {
    display: inline-block;
    width: 100%;
}
.google-pay-checkout button {
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
    background-origin: content-box;
    border: none;
}
.payment_box.payment_method_<?php echo $gateway_id; ?> {
    display: none !important;
}
</style>
<script type="text/javascript">
jQuery(function($) {
        const hidePlaceOrderGateways = <?php echo json_encode($hide_place_order_button_gateways); ?>;
        const googlePayClient = GooglePayClient.initialize({testEnv: <?php echo $is_test_environment ? 'true' : 'false'; ?>});
        var form_checkout = $('form.checkout');
        
        form_checkout.on( 'checkout_place_order_<?php echo $gateway_id; ?>', function() {
                return false;
        });
        
        function blockOnSubmit( $form ) {
			var isBlocked = $form.data( 'blockUI.isBlocked' );

			if ( 1 !== isBlocked ) {
				$form.block({
					message: null,
					overlayCSS: {
						background: '#fff',
						opacity: 0.6
					}
				});
			}
		}
		
		function submit_error( error_message ) {
			$( '.woocommerce-NoticeGroup-checkout, .woocommerce-error, .woocommerce-message' ).remove();
			form_checkout.prepend( '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' + error_message + '</div>' ); // eslint-disable-line max-len
			form_checkout.removeClass( 'processing' ).unblock();
			form_checkout.find( '.input-text, select, input:checkbox' ).trigger( 'validate' ).trigger( 'blur' );
			scroll_to_notices();
			$( document.body ).trigger( 'checkout_error' , [ error_message ] );
		}
		
		function scroll_to_notices() {
			var scrollElement = $( '.woocommerce-NoticeGroup-updateOrderReview, .woocommerce-NoticeGroup-checkout' );

			if ( ! scrollElement.length ) {
				scrollElement = form_checkout;
			}
			$.scroll_to_notices( scrollElement );
		}
        
        googlePayClient.addGooglePayButton({
            buttonColor: '<?php echo $google_button_style; ?>',
            buttonType: 'plain',
            buttonSizeMode: 'fill',
            onClick: googlePayClicked,
        });
        
        function googlePayClicked() {
            $('form.checkout').submit();
            blockOnSubmit(form_checkout);
            window.GooglePayClient = googlePayClient;
            setTimeout(function() {
                var returnValue = false;
                $.ajax({
                    type:		'POST',
                    url:		wc_checkout_params.checkout_url,
                    data:		$('form.checkout').serialize(),
                    dataType:   'json',
                    async:      false,
                    success:	function( result ) {
                        if ( result.google ) {
                            returnValue = result.google;
                        } else if ( result.messages ) {
                            submit_error( result.messages );
                        } else {
                            submit_error( '<div class="woocommerce-error">' + wc_checkout_params.i18n_checkout_error + '</div>' ); // eslint-disable-line max-len
                        }
                    },
                    error:	function( jqXHR, textStatus, errorThrown ) {
                        submit_error( '<div class="woocommerce-error">' + errorThrown + '</div>' );
                    }
                });
                
                if (returnValue) {
                    handleResponse(returnValue);
                    $('form.checkout').removeClass( 'processing' ).unblock();
                    // Display the Google Pay payment sheet
                    googlePayClient.createGooglePayPayment();
                }
            }, 500);
        }

        // Function to handle the response data
        function handleResponse(responseData) {
            // Create responseParams object using var keyword
            const responseParams = {
                access_token: responseData.mobile_access_token,
                api_username: responseData.api_username,
                account_name: responseData.account_name,
                payment_reference: responseData.payment_reference,
                country_code: responseData.country_code,
                currency_code: responseData.currency_code,
                payment_link: responseData.payment_link,
                amount: `${responseData.total_amount}`,
            }

            googlePayClient.configure(responseParams)
        }
        
        $('body').on('updated_checkout', function() {
                checkGooglePaymentMethod();
        });
        
        function checkGooglePaymentMethod() {
            var payment_method = $('input[name=payment_method]:checked').val();
            if(payment_method == '<?php echo $gateway_id; ?>') {
                var googlePayButton = $("#google-pay-container").detach();
                $('button#place_order').after(googlePayButton);
                $('button#place_order').hide();
            } else {
                var googlePayButton = $("#google-pay-container").detach();
                $('#google-pay-container-parent').html(googlePayButton);
                if(!hidePlaceOrderGateways.includes(payment_method)) {
                    $('button#place_order').show();
                }
            }
        }
        form_checkout.on( 'click', 'input[name=payment_method]', function() {
                checkGooglePaymentMethod();
        });
});
</script>