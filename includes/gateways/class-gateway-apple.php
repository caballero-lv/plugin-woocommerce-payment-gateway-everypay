<?php

namespace Everypay;

if(!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly.

/**
 * WooCommerce EveryPay.
 *
 * @class   Gateway_Apple
 * @extends Gateway
 * @version 1.2.0
 * @package WooCommerce Payment Gateway Everypay/Includes
 * @author  EveryPay
 */
class Gateway_Apple extends Gateway
{
    /**
     * @var string
     */
    public $id = 'everypay_apple';

    /**
     * Constructor for the gateway.
     *
     * @access public
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Setup gateway.
     *
     * @return void
     */
    protected function setup()
    {
        $this->title = $this->get_option('title_apple');
    }

    /**
     * Display payment method info.
     *
     * @return void
     */
    public function payment_fields()
    {
        $template_args = [
            'gateway_id' => $this->id,
            'apple_button_style' => $this->get_apple_button_style(),
            'apple_js_file' => $this->get_apple_js_file(),
            'hide_place_order_button_gateways' => ['everypay_apple', 'everypay_google'],
        ];
        wc_get_template('payment-methods-apple.php', $template_args, '', Base::get_instance()->template_path());
    }

    /**
     * Get bank methods.
     *
     * @return object[]
     */
    public function get_payment_methods()
    {
        return Helper::filter_payment_methods(parent::get_payment_methods(), Gateway::TYPE_APPLE);
    }
}