<?php

namespace Everypay;

if(!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly.

/**
 * WooCommerce EveryPay.
 *
 * @class   Gateway_Google
 * @extends Gateway
 * @version 1.2.0
 * @package WooCommerce Payment Gateway Everypay/Includes
 * @author  EveryPay
 */
class Gateway_Google extends Gateway
{
    /**
     * @var string
     */
    public $id = 'everypay_google';

    /**
     * Constructor for the gateway.
     *
     * @access public
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Setup gateway.
     *
     * @return void
     */
    protected function setup()
    {
        $this->title = $this->get_option('title_google');
    }

    /**
     * Display payment method info.
     *
     * @return void
     */
    public function payment_fields()
    {
        $template_args = [
            'gateway_id' => $this->id,
            'google_button_style' => $this->get_google_button_style(),
            'google_js_file' => $this->get_google_js_file(),
            'is_test_environment' => $this->is_google_sandbox(),
            'hide_place_order_button_gateways' => ['everypay_apple', 'everypay_google'],
        ];
        wc_get_template('payment-methods-google.php', $template_args, '', Base::get_instance()->template_path());
    }

    /**
     * Get bank methods.
     *
     * @return object[]
     */
    public function get_payment_methods()
    {
        return Helper::filter_payment_methods(parent::get_payment_methods(), Gateway::TYPE_GOOGLE);
    }
}